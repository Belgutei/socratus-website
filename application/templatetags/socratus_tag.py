from django import template

register = template.Library()


@register.simple_tag
def file_type(file_name):
    print('file_name : ' + str(file_name))
    type = file_name.split('.')[-1]
    return type

@register.filter(name='alignment')
def alignment(counter):
    try:
        if (counter % 2) == 0:
            return True
    except Exception:
        pass
    return False
