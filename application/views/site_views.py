import json
import urllib
import urllib.request

from PIL import Image
from django.conf import settings
from django.contrib import messages
from django.core import mail
from django.core.mail import EmailMessage
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils import timezone
from django.views.generic import View
from django.core.mail import send_mail


from application.models import *
from application.render import Render
from web.settings import PDF_ROOT

connection = mail.get_connection()


def index(request):
    newses = News.objects.filter().order_by('created_at')
    tags = NewsTag.objects.all()
    abouts = Content.objects.filter(content_id='about', publish=1).order_by('created_at').last()
    songon = Content.objects.filter(content_id='songon', publish=1).order_by('created_at').last()
    bsbb = Content.objects.filter(content_id='bsbb', publish=1).order_by('created_at').last()
    tsene = Content.objects.filter(content_id='tsene', publish=1).order_by('created_at').last()
    bodlogo = Content.objects.filter(content_id='bodlogo', publish=1).order_by('created_at').last()
    home_slide = Content.objects.filter(content_id='slide', publish=1).order_by('id')
    business = Content.objects.filter(content_id='business', publish=1).order_by('id').last()
    shiidel = Content.objects.filter(content_id='shiidel', publish=1).order_by('id').last()
    zorilgotoi = Content.objects.filter(content_id='zorilgotoi', publish=1).order_by('id').last()
    mservices = Content.objects.filter(content_id='mservices', publish=1).order_by('id')
    features = Content.objects.filter(content_id='features', publish=1).order_by('id')
    boijigch = Drow.objects.filter(publish=1).order_by('created_at')
    algorithm = Algorithms.objects.filter(publish=1).order_by('id')
    process = Content.objects.filter(content_id='process', publish=1).order_by('created_at').last()
    rising = Content.objects.filter(content_id='rising', publish=1).order_by('id')
    timeline = Content.objects.filter(content_id='timeline', publish=1).order_by('created_at').last()
    mentors = Quotes.objects.filter(publish='1').order_by('id')
    fin_sums = Drow.objects.filter(publish='1')
    sums = 0
    for fin_sum in fin_sums:
        sums += int(fin_sum.finance)
    sums = sums / 1000000
    len_drows = len(fin_sums)
    return render(request, 'new/index1.html',  locals())

	
def handler404(request, exception):
    return render(request, 'site/404.html', locals())


def handler500(request, exception):
    return render(request, 'site/505.html', locals())


def about(request):
    abouts = Content.objects.filter(content_id='about', publish=1).order_by('created_at').last()
    return render(request, 'new/about1.html', locals())

def our_team(request):
    members = OurTeam.objects.filter(teamMember_id='members').order_by('created_at')

    return render(request, 'new/about2.html', locals())

def our_philosophy(request):
    zarchim = Philosophy.objects.filter(publish=1).order_by('id')
    zorilgo = Principle.objects.filter(publish=1).order_by('id')

    return render(request, 'new/about3.html', locals())


def grow(request):
    params={}
    main_text = Content.objects.filter(content_id='boijigch', publish=1).order_by('created_at').last()
    main_text = Content.objects.filter(content_id='boijigch', publish=1).order_by('created_at').last()
    grows = DrowID.objects.filter().order_by('created_at')
    params.update({'tag__id': 4})
    newses = News.objects.filter(**params).order_by('created_at')
    boijigch = Drow.objects.filter().order_by('created_at')
    return render(request, 'new/portfolio.html', locals())


def slider(request):
    return render(request,'site/slider.html')


def single_grow(request, grow_id):
    drow = Drow.objects.filter(id=grow_id).last()
    drows = Drow.objects.filter().order_by('created_at')
    drow_slide = Photo.objects.filter().order_by('image')
    return render(request, 'new/portfolio-single.html', locals())


def image(request):

    return render(request, 'site/image.html')


def bodlogo(request):
    bodlogo=Content.objects.filter(content_id='bodlogo', publish=1).order_by('created_at').last()
    return render(request, 'site/bodlogo.html',locals())


def investor(request):
    investor=Content.objects.filter(content_id='investor', publish=1).order_by('created_at').last()
    return render(request, 'site/hurungu.html', locals())


def mentor(request):
    services = Service.objects.filter().order_by('created_at')
    mentor_desc = OurTeam.objects.filter().order_by('created_at')
    mentors = Quotes.objects.filter().order_by('created_at')
    return render(request, 'site/mentor.html', locals())


def mentor_list(request, office_id):

    return render(request, 'site/mentorlist.html', {'a': office_id})


def news(request):
    # needs = NeedFiles.objects.filter().order_by('created_at')
    # links = NeedLinks.objects.filter().order_by('created_at')
    # news_type = NewsType.objects.filter().order_by('id')
    # type_get = request.GET['type']
    # tag_get = request.GET['tag']
    params = {}

    # if request.GET['type'] and request.GET['tag']:
    #     params.update({'type__id': request.GET['type'], 'tag__id': request.GET['tag']})
    # elif request.GET['tag']:
    #     params.update({'tag__id': request.GET['tag']})
    # elif request.GET['type']:
    #     params.update({'type__id': request.GET['type']})
    newses = News.objects.filter(**params).order_by('created_at')
    return render(request, 'new/blog.html', locals())


def single_news(request, article_id):
    needs = NeedFiles.objects.filter().order_by('created_at')
    links = NeedLinks.objects.filter().order_by('created_at')
    tags = NewsTag.objects.all()
    news_type = NewsType.objects.filter().order_by('id')
    links = NeedLinks.objects.filter().order_by('created_at')
    newses = News.objects.filter(id=article_id).last()
    images = NewsImages.objects.filter(news=newses)
    return render(request, 'new/blog-single.html', locals())


def incubator(request):
    mentor_desc = OurTeam.objects.filter().order_by('created_at').last()
    incubator1 = Incubator.objects.filter().last()
    values = Ivalue.objects.filter().order_by('created_at')
    return render(request, 'new/incubator.html', locals())

def incubator_request(request):
    zuvluguu=Content.objects.filter(content_id='zuvluguu').order_by('id').last()
    return render(request, 'site/incubator_request.html',locals())

def biz_stepup(request):
    biz_contents = bizStepUp.objects.all()
    return render(request, 'new/biz-stepup.html', locals())
    
def i_stepup(request):
    i_contents = iStepUp.objects.filter(publish=1).order_by()
    ontslog = iStepUp.objects.filter(istep_id='ifeatures').order_by('id')             
    return render(request, 'new/i-stepup.html', locals())

def coworking(request):
    business_sector = Pitchday.objects.filter(publish=1).order_by()
    zarchim = Pitchday.objects.filter(pitch_id='pzarchim').order_by('id')
    return render(request, 'new/coworking.html', locals())


def incubator_request2(request):

    data = request.POST
    print('request.POST : ' + str(request.POST))
    return render(request, 'site/incubator_request2.html', locals())


def check_email(request):
    check_email = RequestForm.objects.filter(c1=request.GET['email']).first()
    if check_email:
        return HttpResponse('ok')
    else:
        return HttpResponse('not')


def check_phone(request):
    check_email = RequestForm.objects.filter(c2=request.GET['phone']).first()
    if check_email:
        return HttpResponse('ok')
    else:
        return HttpResponse('not')


def results(request):

    return render(request, 'site/result.html', locals())


def gcapcha(request):

    recaptcha_response = request.POST.get('g-recaptcha-response')
    url = 'https://www.google.com/recaptcha/api/siteverify'
    values = {
        'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
        'response': recaptcha_response
    }
    data = urllib.parse.urlencode(values).encode()
    req = urllib.request.Request(url, data=data)
    check_email=RequestForm.objects.filter(c2=request.POST['c2'])
    response = urllib.request.urlopen(req)
    result = json.loads(response.read().decode())
    if request.POST['title']:
        if result['success']:
            if check_email:
                messages.error(request, 'Таны хүсэлтийг аль хэдийн бүртгэсэн байна.')
                return render(request, 'site/burtgesen.html', locals())
            else:
                test = RequestForm(
                                bi1=request.POST.get("bi1"),
                                bi2=request.POST.get("bi2"),
                                bi3=request.POST.get("bi3"),
                                bi4=request.POST.get("bi4"),
                                bi5=request.POST.get("bi5"),
                                bi6=request.POST.get("bi6"),
                                sh1=request.POST.get("sh1"),
                                sh2=request.POST.get("sh2"),
                                sh3=request.POST.get("sh3"),
                                sh4=request.POST.get("sh4"),
                                bm1=request.POST.get("bm1"),
                                bm2=request.POST.get("bm2"),
                                t1=request.POST.get("t1"),
                                t2=request.POST.get("t2"),
                                t3=request.POST.get("t3"),
                                t4=request.POST.get("t4"),
                                bt1=request.POST.get("bt1"),
                                bt2=request.POST.get("bt2"),
                                op1=request.POST.get("op1"),
                                op2=request.POST.get("op2"),
                                in1=request.POST.get("in1"),
                                in2=request.POST.get("in2"),
                                in3=request.POST.get("in3"),
                                co1=request.POST.get("co1"),
                                co2=request.POST.get("co2"),
                                la1=request.POST.get("la1"),
                                ri1=request.POST.get("ri1"),
                                ri2=request.POST.get("ri2"),
                                ri3=request.POST.get("ri3"),
                                c1=request.POST.get("c1"),
                                c2=request.POST.get("c2"),
                )
                test.save()
                messages.success(request, 'Танд баярлалаа.Бид таны хүсэлтийг хүлээн авлаа.')
                return render(request, 'site/success_request.html', locals())
        else:
            messages.error(request, 'GOOGLE CAPCHA бөглөгдөөгүй эсвэл хоосон байна.Та заавал бөглөн үү')
            return render(request, 'site/wrong_request.html', locals())
    else:
            redirect('/submission')


def contact(request):
    if request.method == 'GET':
        contact = Contact.objects.filter(active=True).last()    
        return render(request, 'new/contact.html', locals())

    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        mobile = request.POST['mobile']
        title = request.POST['title']
        message = request.POST['message']

        print(f'{name}')
        print(f'{email}')
        print(f'{mobile}')
        print(f'{title}')
        print(f'{message}')

        send_mail(title, 'email: ' + email + ' - ' + message, settings.EMAIL_HOST_USER, ['belguteibattulga@gmail.com'])

    return render(request, 'new/contact.html')


def test(request):
    image=Image.open('http://localhost:8000/media/documents/2018/09/12/Screenshot_1.png')
    return render(request, 'site/test.html', locals())


def sendmail(request):
    subject = request.POST.get('name')
    emails = request.POST.get('email')
    message = request.POST.get('message')
    phone=request.POST.get('phone')
    print(subject)
    print(emails)
    print(message)
    email = EmailMessage(subject, '\r\nИмэйл: ' + emails + '\r\nИмэйл: ' + phone + '\r\nЗахидал: ' + message, to=['belgutei@tussolution.mn'])
    email.send()

    messages.success(request, 'Амжилттай илгээгдлээ.Танд баярлалаа')
    return redirect('new/contact.html')


class Pdf(View):

    def get(self, request,grow_id):
        datas = RequestForm.objects.filter(id=grow_id)
        today = timezone.now()
        pdf_root = PDF_ROOT
        #return render(request, 'site/pdf.html', locals())
        return Render.render('site/pdf.html', locals())
