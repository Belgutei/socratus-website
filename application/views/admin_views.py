import datetime

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core import mail
from django.core.paginator import Paginator
from django.shortcuts import render, redirect
from django.views import View

connection = mail.get_connection()
from django.core.mail import EmailMessage

from application.forms import ContentForm, SeoForm, ImageForm
from application.forms import TypeForm, Content, ImageModel, Seo, Type

now = datetime.datetime.now()


class AdminViewPage(View):
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            content = Content.objects.filter()
            paginator = Paginator(content, 20)
            page = request.GET.get('page')
            context = paginator.get_page(page)
            return render(request, 'contents.html', {'contents': context})
        else:
            return render(request, self.template_name, None)

    def post(self, request, *args, **kwargs):
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('/admin_panel')
        else:
            messages.warning(request, 'Хэрэглэгчийн нэр эсвэл нууц үг буруу байна.')
            return render(request, 'login.html', None)


class AdminContentAdd(View, PermissionRequiredMixin):
    def get(self, request, *args, **kwargs):
        form = ContentForm()
        context = Type.objects.filter()
        seo = Seo.objects.filter()

        data = {
            'types': context,
            'form': form,
            'seo': seo
        }
        return render(request, 'content_add.html', data)

    def post(self, request, *args, **kwargs):
        form = ContentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/admin_panel')
        return None


class AdminContentEdit(View, LoginRequiredMixin, PermissionRequiredMixin):

    def get(self, request, question_id, *args, **kwargs):
        if request.user.is_authenticated:
            instance = Content.objects.get(id=question_id)
            form = ContentForm(instance=instance)
            content = Content.objects.filter(id=question_id)
            seo = Seo.objects.filter()
            types = Type.objects.filter()
            images = ImageModel.objects.filter(image_id=question_id)
            return render(request, 'content_edit.html',
                          {'types': types, 'content': content, 'form': form, 'seo': seo, 'images': images})
        else:
            return render(request, self.template_name, None)


    def post(self, request, question_id, *args, **kwargs):
        if request.user.is_authenticated:
            instance = Content.objects.get(id=question_id)
            form = ContentForm(request.POST, request.FILES, instance=instance)
            if form.is_valid():
                form.save()
                return redirect('/admin_panel')

        else:
            return render(request, self.template_name, None)


class AdminContentDelete(View, LoginRequiredMixin, PermissionRequiredMixin):

    def get(self, request, question_id, *args, **kwargs):
        if request.user.is_authenticated:
            Content.objects.filter(id=question_id).delete()
            return redirect('/admin_panel')
        else:
            return render(request, self.template_name, None)

    def post(self, request, *args, **kwargs):
            return None


def logout_view(request):
    logout(request)
    return redirect('/admin_panel')


def ContentImageAdd(request):
    return None


def simple_upload(request):
    if request.method == 'POST':
        for image in request.FILES.getlist('image'):

            form = ImageForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
            return redirect('/admin_panel')


def ContentImageAdd_delete(request, question_id):
    ImageModel.objects.filter(id=question_id).delete()
    return redirect('/admin_panel/')


def sendmail(request):
    subject = request.POST.get('name')
    emails = request.POST.get('email')
    message = request.POST.get('name') + ' хэрэглэгч дараах хүсэлтийг илгээсэн байна.\n'+ request.POST.get('message')+ ' \n Холбогдох хаяг: '+ request.POST.get('email')
    email = EmailMessage(subject, message, to=['shinegerel@reachpoint.mn'])
    email.send()

    messages.success(request, 'Амжилттай илгээгдлээ.Танд баярлалаа')

    return redirect('/site/contact')


def types(request):
    if request.user.is_authenticated:
        content = Type.objects.filter()
        paginator = Paginator(content, 20)
        page = request.GET.get('page')
        contents = paginator.get_page(page)
        return render(request, 'types.html', {'contents': contents})


def type_add(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = TypeForm(request.POST, request.FILES)

            if form.is_valid():
                form.save()
                return redirect('/admin_panel/type')
        else:
            types = Type.objects.filter()

        return render(request, 'type_add.html', {
            'types': types,

        })


def type_edit(request, question_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            instance = Type.objects.get(id=question_id)
            form = TypeForm(request.POST, request.FILES, instance=instance)
            if form.is_valid():
                form.save()
                return redirect('/admin_panel/type')
        else:
            content = Type.objects.filter(id=question_id)
            types = Type.objects.filter()
        return render(request, 'type_edit.html', {
            'types': types,
            'content': content
        })


def type_delete(request, question_id):
    if request.user.is_authenticated:
        Type.objects.filter(id=question_id).delete()
        return redirect('/admin_panel/type')


def seo(request):
    if request.user.is_authenticated:
        content = Seo.objects.filter()
        paginator = Paginator(content, 20)
        page = request.GET.get('page')
        contents = paginator.get_page(page)
        return render(request, 'seo.html', {'contents': contents})


def seo_add(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = SeoForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect('/admin_panel/seo')
        else:

            return render(request, 'seo_add.html')


def seo_edit(request, question_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            instance = Seo.objects.get(id=question_id)
            form = SeoForm(request.POST, request.FILES, instance=instance)
            if form.is_valid():
                form.save()
                return redirect('/admin_panel/seo')
        else:
            content = Seo.objects.filter(id=question_id)
            return render(request, 'seo_edit.html', {
                'content': content
            })


def seo_delete(request, question_id):
    if request.user.is_authenticated:
        Seo.objects.filter(id=question_id).delete()
        return redirect('/admin_panel/seo')


def image(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = ImageForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
            return redirect('/admin_panel/image')
        else:
            form = ContentForm()
            context = ImageModel.objects.filter()
            return render(request, 'image.html', {'context': context, 'form': form})


def image_delete(request, question_id):
    if request.user.is_authenticated:
        ImageModel.objects.filter(id=question_id).delete()
        return redirect('/admin_panel')
