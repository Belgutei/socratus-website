# Generated by Django 2.0.6 on 2020-01-07 03:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0041_principle_publish'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Contacts',
            new_name='Contact',
        ),
    ]
