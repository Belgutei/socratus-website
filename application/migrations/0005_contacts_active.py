# Generated by Django 2.0.6 on 2019-12-02 09:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0004_contacts'),
    ]

    operations = [
        migrations.AddField(
            model_name='contacts',
            name='active',
            field=models.BooleanField(default=False, verbose_name='Гарчиг'),
        ),
    ]
