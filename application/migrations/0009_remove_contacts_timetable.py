# Generated by Django 2.0.6 on 2019-12-10 02:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0008_auto_20191210_1023'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contacts',
            name='timetable',
        ),
    ]
