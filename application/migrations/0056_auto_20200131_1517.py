# Generated by Django 2.0.6 on 2020-01-31 07:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0055_auto_20200131_1509'),
    ]

    operations = [
        migrations.AlterField(
            model_name='drow',
            name='portfolio',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='application.Photo', verbose_name='Single'),
        ),
    ]
