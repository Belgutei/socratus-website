# Generated by Django 2.0.6 on 2019-12-10 02:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0006_contacts_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contacts',
            name='map',
            field=models.CharField(blank=True, max_length=511, null=True, verbose_name='Газрын зураг'),
        ),
        migrations.AlterField(
            model_name='contacts',
            name='phone',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Утас'),
        ),
        migrations.AlterField(
            model_name='contacts',
            name='short_desc',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Богино мэдээллэл'),
        ),
    ]
