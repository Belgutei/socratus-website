from translations.models import Translatable
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill

pub_status = (
    ('0', 'Үгүй'),
    ('1', 'Тийм'),
)
news_type = (
    ('slide', 'Слайд зураг'),
    ('about', 'Бидний тухай'),
    ('songon', 'Сонгон шалгарулах'),
    ('bsbb', 'Бизнесийн систем бий болгох'),
    ('tsene', 'Үнэ цэнэ бүтээх'),
    ('business', 'Хүний хэрэгцээг хангах Бизнес санаа'),
    ('shiidel', 'Мэдлэгт суурилсан Шийдэл-Инноваци'),
    ('zorilgotoi', 'Бизнесийн нэгдмэл Зорилготой баг'),
    ('bodlogo', 'Бодлого'),
    ('investor', 'Хөрөнгө оруулагч'),
    ('boijigch', 'Бойжигч'),
    ('zuvluguu', 'Хүсэлтийн зөвлөгөө'),
    ('mservices', 'Гол үйлчилгээ'),
    ('features','Онцлог хэв шинж'),
    ('process', 'Алгоритмын тайлбар'),
    ('rising', 'Он цагийн хэлхээс'),
    ('timeline', 'Он цагийн тайлбар'),
    ('members', 'Багийн Гишүүд'),
    ('mentors', 'Quotes'),
    ('zarchim', 'Ажилтнуудын баримтлах зарчим'),
    ('zorilgo', 'Зорилго'),
    ('ifeatures', 'iStepUp онцлог'),
    ('pzarchim', 'Pitchday Баримтлах зарчим')
    
)
class bizStepUp(models.Model):
    class Meta:
        verbose_name = 'bizStepUp'
        verbose_name_plural = 'bizStepup'
    title = models.CharField('Гарчиг', max_length=100, null=True, blank=True)
    short_desc = models.TextField('Богино мэдээллэл', null=True, blank=True)
    description = RichTextUploadingField('Дэлгэрэнгүй мэдээлэл', blank=True)
    image = models.FileField('Зураг', upload_to='documents/%Y/%m/%d/', blank=True)

    def __str__(self):
        return self.title

class iStepUp(models.Model):
    class Meta:
        verbose_name = 'iStepUp'
        verbose_name_plural = 'iStepup'
    istep_id = models.CharField('Төрөл', max_length=100, choices=news_type, null=True, blank=True)
    title = models.CharField('Гарчиг', max_length=100, null=True, blank=True)
    short_desc = models.TextField('Богино мэдээллэл', null=True, blank=True)
    description = RichTextUploadingField('Дэлгэрэнгүй мэдээлэл', blank=True)
    publish = models.CharField('Нийтлэх эсэх', max_length=1, choices=pub_status, null=True, blank=True)
    image = models.FileField('Зураг', upload_to='documents/%Y/%m/%d/', blank=True)

    def __str__(self):
        return self.title

class Pitchday(models.Model):
    class Meta:
        verbose_name = 'Pitchday'
        verbose_name_plural = 'Pitchday'
    pitch_id = models.CharField('Төрөл', max_length=100, choices=news_type, null=True, blank=True)
    title = models.CharField('Гарчиг', max_length=100, null=True, blank=True)
    short_desc = models.TextField('Богино мэдээллэл', null=True, blank=True)
    description = RichTextUploadingField('Дэлгэрэнгүй мэдээлэл', blank=True)
    image = models.FileField('Зураг', upload_to='documents/%Y/%m/%d/', blank=True)
    publish = models.CharField('Нийтлэх эсэх', max_length=1, choices=pub_status, null=True, blank=True)

    def __str__(self):
        return self.title

class Logo(models.Model):
    class Meta:
        verbose_name = 'Хамтрагч байгууллагын лого'
        verbose_name_plural = 'Хамтрагч байгууллагын лого'

    title = models.CharField('Гарчиг', max_length=100, null=True, blank=True)

    image = models.FileField('Лого', upload_to='documents/%Y/%m/%d/', blank=True)

    def __str__(self):
        return self.title

class Philosophy(models.Model):
    class Meta:
        verbose_name = 'Философи'
        verbose_name_plural = 'Философи'
    title = models.CharField('Гарчиг', max_length=150)
    short_desc = models.TextField('Богино мэдээллэл', null=True, blank=True)
    description = RichTextUploadingField('Дэлгэрэнгүй мэдээлэл', blank=True)
    publish = models.CharField('Нийтлэх эсэх', max_length=1, choices=pub_status, null=True)
    image = models.FileField('Зураг', upload_to='documents/%Y/%m/%d/', blank=True)
    phil_id = models.CharField('Төрөл', max_length=100, null=True, choices=news_type)

    def __str__(self):
        return self.title

class Principle(models.Model):
    class Meta:
        verbose_name = 'Зорилго'
        verbose_name_plural = 'Зорилго'
    title = models.CharField('Гарчиг', max_length=150)
    publish = models.CharField('Нийтлэх эсэх', max_length=1, choices=pub_status, null=True)
    short_desc = models.TextField('Богино мэдээллэл', null=True, blank=True)
    description = RichTextUploadingField('Дэлгэрэнгүй мэдээлэл', blank=True)
    image = models.FileField('Зураг', upload_to='documents/%Y/%m/%d/', blank=True)

    def __str__(self):
        return self.title


class Incubator(models.Model):
    class Meta:
        verbose_name = 'Инкубатор'
        verbose_name_plural = 'Инкубатор'

    title = models.CharField('Гарчиг', max_length=150)
    publish = models.CharField('Нийтэлсэн эсэх', max_length=1, choices=pub_status, null=True)
    iframe_name = models.CharField('Видео EMBED гарчиг', blank=True, max_length=255)
    iframe_desc = models.CharField('Видео EMBED тайлбар', blank=True, max_length=255)
    iframe = models.CharField('Видео EMBED линк', blank=True, max_length=255)
    description = RichTextUploadingField('Дэлгэрэнгүй мэдээлэл')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    


class Ivalue(models.Model):
    class Meta:
        verbose_name = 'Үр шим'
        verbose_name_plural = 'Үр шим'

    incubator = models.ForeignKey(Incubator, on_delete=models.CASCADE, null=True, blank=True)
    description = RichTextUploadingField('Дэлгэрэнгүй мэдээлэл', blank=True)
    image = models.FileField('Зураг', upload_to='documents/%Y/%m/%d/')
    photos = ImageSpecField(source='image',
                            processors=[ResizeToFill(350, 165)],
                            format='JPEG',
                            options={'quality': 100})
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Content(models.Model):
    class Meta:
        verbose_name = 'Агуулга'
        verbose_name_plural = 'Агуулга'

    title = models.CharField('Гарчиг', max_length=100)
    content_id = models.CharField('Төрөл', max_length=100, choices=news_type)
    image = models.FileField(upload_to='documents/%Y/%m/%d/', null=True, blank=True)
    publish = models.CharField('Нийтэлсэн эсэх', max_length=1, choices=pub_status)
    slide = models.CharField('Слайд линк', max_length=255, null=True, blank=True)
    short_desc = models.TextField('Богино мэдээллэл', null=True, blank=True)
    description = RichTextUploadingField('Дэлгэрэнгүй мэдээлэл', blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    style = models.CharField('Товчлуур стайл', max_length=255, null=True, blank=True)
    link = models.CharField('Линк нэр', max_length=255, null=True, blank=True)
    buttonRegister = models.CharField('Товчлуурын нэр', max_length=255, null=True, blank=True)
    
    def __str__(self):
        return self.title


class RequestForm(models.Model):
    class Meta:
        verbose_name = 'Хүсэлт'
        verbose_name_plural = 'Хүсэлт'

    bi1 = models.TextField('Тухайн бизнес санаагаар ямар асуудлыг шийдэхээр зорьсон вэ?', null=True, blank=True)
    bi2 = models.TextField('Асуудал шийдэгдснээр хэний, ямар хэрэгцээг хангах вэ?', null=True, blank=True)
    bi3 = models.TextField('Тухайн шийдвэрлэхийг зорьж буй асуудал нь зайлшгүй шийдвэрлэх шаардлагатай асуудал мөн үү?',
                           null=True, blank=True)
    bi4 = models.TextField(
        'Одоогийн нөхцөлд бизнес санааны шийдлээр хангагдах хэрэгцээгээ хэрэглэгчид хэрхэн хангаж байна вэ?', null=True,
        blank=True)
    bi5 = models.TextField('Зах зээлийн хэмжээ?', null=True, blank=True)
    bi6 = models.TextField('Зах зээлийн өсөлт?', null=True, blank=True)

    sh1 = models.TextField('Бизнес санаагаар санал болгож буй шийдэл юу вэ?', null=True, blank=True)
    sh2 = models.TextField('Бизнес санааны шийдлийн харьцангуй давуу тал юу вэ? Бусад шийдлүүдтэй харьцуулахад.',
                           null=True, blank=True)
    sh3 = models.TextField('Шийдлийн хөгжүүлэлт төлөвлөлтийнхөө ямар түвшинд байгаа вэ?', null=True, blank=True)
    sh4 = models.TextField('Шийдлийг зах зээлд нэвтрүүлэхэд хэдий хэр хугацаа шаардах вэ?', null=True, blank=True)

    bm1 = models.TextField('Бизнесийн загвар юу вэ? Хэрхэн ажиллах вэ?', null=True, blank=True)
    bm2 = models.TextField('Бизнес санааг хэрэгжүүлэхэд ямар оролцогч талууд зайлшгүй шаардлагатай вэ?', null=True,
                           blank=True)

    t1 = models.TextField('Бизнес санаагаа хэрэгжүүлэх мэргэжлийн мэдлэг, чадвар байгаа юу?', null=True, blank=True)
    t2 = models.TextField('Өмнө нь ямар нэгэн төсөл хэрэгжүүлсэн туршлагатай байна уу?', null=True, blank=True)
    t3 = models.TextField('Бизнес санаагаа хэрэгжүүлэхийн тулд яагаад одоогийн баг бүрдсэн вэ?', null=True, blank=True)
    t4 = models.TextField('Яагаад танай баг энэ бизнесийг хийхээр зорьсон вэ? ', null=True, blank=True)

    bt1 = models.TextField(
        'Бизнес санааг хэрэгжүүлэхэд шаардлагатай суурь нөхцлүүд /техник технологийн хөгжил, хэрэглэгчдийн хэрэглээний зан төлөв гэх мэт/ сүүлийн жилүүдэд хэрхэн өөрчлөгдсөн вэ?',
        null=True, blank=True)
    bt2 = models.TextField('Эдгээр өөрчлөлтүүд нь бизнес санааг хэрэгжүүлэх боломжийг олгосон уу? Яагаад?', null=True,
                           blank=True)

    op1 = models.TextField('Бизнес санаагаар бий болох бүтээгдэхүүн, үйлчилгээний төрлийг өргөтгөх боломжтой юу?',
                           null=True, blank=True)
    op2 = models.TextField(
        'Бизнес санаагаар бий болох бүтээгдэхүүн үйлчилгээг өөр зах зээлд /өөр улс оронд/ гаргах боломжтой юу?',
        null=True, blank=True)

    in1 = models.TextField('Хөрөнгө оруулалтын зарцуулалт?', null=True, blank=True)
    in2 = models.TextField(
        'Хэдий хэмжээний хөрөнгө оруулалтаар хугарлын цэгтээ /компанийн орлого үйл ажиллагааны зардлаа нөхөх/ хүрэх вэ?',
        null=True, blank=True)
    in3 = models.TextField(
        'Хугарлын цэгтээ /компанийн орлого үйл ажиллагааны зардлаа нөхөх/ хүрэхэд хэдий хугацаа шаардах вэ?', null=True,
        blank=True)

    co1 = models.TextField('Бизнес санаатай ижилхэн бизнесийг хэн нэгэн эрхлэж байна уу?', null=True, blank=True)
    co2 = models.TextField('Өөр хэн нэгэн бизнес санааг хэрэгжүүлэх боломжтой юу?', null=True, blank=True)

    la1 = models.TextField('Тусгайлсан ямар нэг хуулийн хүрээнд эсвэл тусгай зөвшөөрөл авч үйл ажиллагаа явуулах уу?',
                           null=True, blank=True)

    ri1 = models.TextField('Бизнес санаагаа хэрэгжүүлэхэд учирч буй саад юу байна вэ?', null=True, blank=True)
    ri2 = models.TextField('Бизнес санаагаа хэрэгжүүлэхэд учирч буй бэрхшээл юу байна вэ?', null=True, blank=True)
    ri3 = models.TextField('Бизнес санаагаа хэрэгжүүлэхэд учирч буй асуудал юу байна вэ?', null=True, blank=True)

    c1 = models.TextField('Компаний үүсгэн байгуулагчийн имэйл', null=True, blank=True)
    c2 = models.TextField('Утасны дугаар', null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.c1) if self.c1 else 'No Email'


class Service(models.Model):
    class Meta:
        verbose_name = 'Ментор үйлчилгээ'
        verbose_name_plural = 'Ментор үйлчилгээ'

    title = models.CharField('Гарчиг', max_length=100)
    slug = models.CharField('Уриа', max_length=100)
    publish = models.CharField('Нийтэлсэн эсэх', max_length=1, choices=pub_status)
    website = models.CharField('Веб сайт', max_length=100)
    description = RichTextUploadingField('Дэлгэрэнгүй мэдээлэл')
    image = models.FileField(upload_to='documents/%Y/%m/%d/')
    photos = ImageSpecField(source='image',
                            processors=[ResizeToFill(350, 165)],
                            format='JPEG',
                            options={'quality': 100})
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title




class Algorithms(models.Model):
    class Meta:
        verbose_name = 'Алгоритм'
        verbose_name_plural = 'Алгоритм'
    subtitle = models.CharField('Алгоритмын нэр', max_length=100, null=True, blank=True)
    tailbar = RichTextUploadingField('Тайлбар', null=True, blank=True)
    publish = models.CharField('Нийтлэх эсэх', max_length=1, choices=pub_status)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.subtitle



class DrowID(models.Model):
    class Meta:
        verbose_name = 'Бойжигчийн төрөл'
        verbose_name_plural = 'Бойжигчийн төрөл'

    title = models.CharField('Гарчиг', max_length=100, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Photo(models.Model):
    class Meta:
        verbose_name = 'Зураг'
        verbose_name_plural = 'Зураг'
    grow_id = models.ForeignKey(DrowID, on_delete=models.CASCADE, verbose_name='Төрөл', null=True)
    image = models.FileField(upload_to='documents/%Y/%m/%d/', null=True, blank=True)


class Drow(models.Model):
    class Meta:
        verbose_name = 'Бойжигч'
        verbose_name_plural = 'Бойжигч'

    title = models.CharField('Гарчиг', max_length=100, null=True, blank=True)
    grow_id = models.ForeignKey(DrowID, on_delete=models.DO_NOTHING, null=True, blank=True, verbose_name='Төрөл')
    portfolio = models.ForeignKey(Photo, null=True, blank=True, on_delete=models.DO_NOTHING, verbose_name='Single')
    header = models.CharField('Толгой гарчиг', max_length=100, null=True, blank=True)
    short_desc = models.CharField('Богино мэдээллэл', max_length=255, null=True, blank=True)    
    facebook = models.CharField('Facebook', max_length=50, blank=True)
    twitter = models.CharField('Twitter', max_length=50, blank=True)
    instagram = models.CharField('Instagram', max_length=50, blank=True)
    publish = models.CharField('Нийтэлсэн эсэх', max_length=1, choices=pub_status, null=True)
    my_slug = models.TextField('Уриа', null=True, blank=True)
    iframe_name = models.CharField('Видео EMBED гарчиг', blank=True, max_length=255)
    iframe_desc = models.CharField('Видео EMBED тайлбар', blank=True, max_length=255)
    iframe_link = models.CharField('Видео EMBED линк', blank=True, max_length=255)
    image = models.FileField(upload_to='documents/%Y/%m/%d/')
    photos = ImageSpecField(source='image',
                            processors=[ResizeToFill(350, 165)],
                            format='JPEG',
                            options={'quality': 100})

    bd = models.CharField('Үйл ажиллагааны чиглэл', max_length=100)
    start_date = models.DateField('Байгуулагдсан огноо', max_length=100)
    finance = models.CharField('Санхүүжилт', max_length=100)
    finance_step = models.CharField('Санхүүжилтийн үе шат', max_length=100)
    director = models.CharField('Гүйцэтгэх захирал', max_length=100)
    contact = models.CharField('Холбоо барих', max_length=100)
    address = models.CharField('Хаяг', max_length=100)
    website = models.CharField('Вебсайт линк', max_length=100)
    iframe = models.TextField('Видео EMBED код', blank=True, max_length=255)
    description = RichTextUploadingField('Дэлгэрэнгүй мэдээлэл', blank=True, )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class VideoEmbed(models.Model):
    class Meta:
        verbose_name = 'Видео хавсралт'
        verbose_name_plural = 'Видео хавсралт'

    title = models.CharField('Гарчиг', max_length=100, null=True, blank=True)
    desc = models.CharField('Тайлбар', max_length=100, null=True, blank=True)
    link = models.CharField('Линк', max_length=200, null=True, blank=True)

    def __str__(self):
        return self.title


class DrowTimeline(models.Model):
    class Meta:
        verbose_name = 'Он цагийн хэлхээс'
        verbose_name_plural = 'Он цагийн хэлхээс'

    grow = models.ForeignKey(Drow, on_delete=models.CASCADE, null=True, blank=True)
    title = models.CharField('Гарчиг', max_length=100)
    ognoo = models.CharField('Огноо', max_length=100)
    description = RichTextUploadingField('Дэлгэрэнгүй мэдээлэл', blank=True, )


class Images(models.Model):
    grow = models.ForeignKey(Drow, on_delete=models.CASCADE, null=True, blank=True)
    image_id = models.IntegerField('ID', null=True, blank=True)
    image = models.FileField(upload_to='documents/%Y/%m/%d/')
    photos = ImageSpecField(source='image',
                            processors=[ResizeToFill(350, 165)],
                            format='JPEG',
                            options={'quality': 100})

    class Meta:
        verbose_name_plural = 'Зурагнууд'


class OurTeam(models.Model):
    class Meta:
        verbose_name = 'Багийн Гишүүд'
        verbose_name_plural = 'Багийн Гишүүд'
    teamMember_id = models.CharField('Төрөл', max_length=100, null=True, blank=True, choices=news_type)
    name = models.CharField('Нэр', max_length=100)
    career = models.CharField('Албан тушаал', max_length=100, null=True, blank=True)
    facebook = models.CharField('Facebook', max_length=50)
    twitter = models.CharField('Twitter', max_length=50, blank=True)
    linkedin = models.CharField('Linkedin', max_length=50, blank=True)
    # MongolForum = models.CharField('Mongol Forum', max_length=50)
    description = models.CharField('Дэлгэрэнгүй мэдээлэл', max_length=255, null=True, blank=True)
    image = models.FileField(upload_to='documents/%Y/%m/%d/')
    photos = ImageSpecField(source='image',
                            processors=[ResizeToFill(270, 270)],
                            format='PNG',
                            options={'quality': 100})
    active = models.BooleanField('Идэвхтэй эсэх', default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Quotes(models.Model):
    class Meta:
        verbose_name = 'Quotes'
        verbose_name_plural = 'Quotes'

    title = models.CharField('Гарчиг', max_length=100, null=True, blank=True)
    quotes_id = models.ForeignKey(OurTeam, on_delete=models.CASCADE, null=True,  blank=True, verbose_name='Төрөл')
    tailbar = models.CharField('Тайлбар', max_length=255, null=True, blank=True)
    publish = models.CharField('Нийтэлсэн эсэх', max_length=1, choices=pub_status)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Coworking(models.Model):
    class Meta:
        verbose_name = 'Дундын оффис'
        verbose_name_plural = 'Дундын оффис'

    name = models.CharField('Гарчиг', max_length=100)
    tailbar = RichTextUploadingField('Тайлбар', null=True, blank=True)
    uniin = models.FileField('Үнийн санал', upload_to='documents/%Y/%m/%d/', null=True, blank=True)

    def __str__(self):
        return self.name


class Coworking_butets(models.Model):
    class Meta:
        verbose_name = 'Давуу тал'
        verbose_name_plural = 'Давуу тал'

    c_id = models.ForeignKey(Coworking, on_delete=models.CASCADE, blank=True, verbose_name='Төрөл')
    name = models.CharField('Гарчиг', max_length=100, null=True, blank=True)
    tailbar = models.TextField('Тайлбар', null=True, blank=True)

    def __str__(self):
        return self.name


class Coworking_bolomj(models.Model):
    class Meta:
        verbose_name = 'Боломжууд'
        verbose_name_plural = 'Боломжууд'

    c_id = models.ForeignKey(Coworking, on_delete=models.CASCADE, blank=True, verbose_name='Төрөл')
    name = models.CharField('Гарчиг', max_length=100, null=True, blank=True)
    tailbar = models.TextField('Тайлбар', null=True, blank=True)
    image = models.FileField(upload_to='documents/%Y/%m/%d/')

    def __str__(self):
        return self.name


class CoworkingPrice(models.Model):
    class Meta:
        verbose_name = 'Үнийн санал'
        verbose_name_plural = 'Үнийн санал'

    c_id = models.ForeignKey(Coworking, on_delete=models.CASCADE, blank=True, verbose_name='Төрөл')
    name = models.CharField('Гарчиг', max_length=100, null=True, blank=True)
    small_name = models.CharField('Гарчигны тайлбар', max_length=100, null=True, blank=True)
    price = models.CharField('Гарчигны тайлбар', max_length=100, null=True, blank=True)
    tailbar = RichTextUploadingField('Тайлбар', null=True, blank=True)

    def __str__(self):
        return self.name


class NewsTag(models.Model):
    class Meta:
        verbose_name = 'Мэдээний шошго'
        verbose_name_plural = 'Мэдээний шошго'

    title = models.CharField('Гарчиг', max_length=100, null=True, blank=True)

    def __str__(self):
        return self.title


class NewsType(models.Model):
    class Meta:
        verbose_name = 'Мэдээний төрөл'
        verbose_name_plural = 'Мэдээний төрөл'

    title = models.CharField('Гарчиг', max_length=100, null=True, blank=True)

    def __str__(self):
        return self.title


class News(Translatable, models.Model):
    title = models.CharField('Гарчиг', max_length=100)
    type = models.ForeignKey(NewsType, on_delete=models.CASCADE, blank=True, verbose_name='Төрөл')
    tag = models.ManyToManyField(NewsTag, blank=True)
    publish = models.CharField('Нийтлэх эсэх', max_length=1, choices=pub_status)
    short_desc = models.CharField('Богино мэдээллэл', max_length=300)
    description = RichTextUploadingField('Дэлгэрэнгүй мэдээлэл')
    image = models.FileField(upload_to='documents/%Y/%m/%d/')
    photos = ImageSpecField(source='image',
                            processors=[ResizeToFill(350, 165)],
                            format='JPEG',
                            options={'quality': 100})
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
        
    class TranslatableMeta:
        fields = ['title', 'short_desc', 'description']

    class Meta:
        verbose_name = 'Блог мэдээлэл'
        verbose_name_plural = 'Блог мэдээлэл'


class NewsImages(models.Model):
    news = models.ForeignKey(News, related_name='images', on_delete=models.CASCADE)
    images = models.FileField(upload_to='news/%Y/%m/%d/')


class NeedFiles(models.Model):
    class Meta:
        verbose_name = 'Хэрэгцээт файл'
        verbose_name_plural = 'Хэрэгцээт файл'

    title = models.CharField('Гарчиг', max_length=100, null=True, blank=True)
    file = models.FileField(upload_to='documents/%Y/%m/%d/')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class NeedLinks(models.Model):
    class Meta:
        verbose_name = 'Хэрэгцээт линк'
        verbose_name_plural = 'Хэрэгцээт линк'

    title = models.CharField('Гарчиг', max_length=100, null=True, blank=True)
    link = models.CharField('Гарчиг', max_length=100, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
        

class Contact(models.Model):
    active = models.BooleanField('Гарчиг', default=False)
    image = models.FileField(upload_to='documents/%Y/%m/%d/', null=True, blank=True)
    title = models.CharField('Гарчиг', max_length=100, null=True, blank=True)
    short_desc = models.CharField('Богино мэдээллэл', max_length=255, null=True, blank=True)
    map = models.CharField('Газрын зураг', max_length=511, null=True, blank=True)
    phone = models.CharField('Утас', max_length=255, null=True, blank=True)
    fax = models.CharField('Факс', max_length=255, null=True, blank=True)
    email = models.CharField('Имайл', max_length=255, null=True, blank=True)

    def __str__(self):
        return self.title




