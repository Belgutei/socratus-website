from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
import xhtml2pdf.pisa as pisa

from web.settings import PDF_ROOT


class Render:

    @staticmethod
    def render(path: str, params: dict):
        params.update({'pdf_root': PDF_ROOT})
        template = get_template(path)
        html = template.render(params)
        response = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)
        if not pdf.err:
            return HttpResponse(response.getvalue(), content_type='application/pdf; charset=utf-8')
        else:
            return HttpResponse("Error Rendering PDF", status=400)
