from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from application.views import site_views
from application import views
from application.views.site_views import Pdf

urlpatterns=[
                path('', site_views.index, name='index1'),
                path('about/', site_views.about, name='about1'),
                path('our_team/', site_views.our_team, name='about2'),
                path('our_philosophy/', site_views.our_philosophy, name='about3'),
                path('image/', site_views.image, name='image'),
                path('grow/', site_views.grow, name='boij'),
                path('mentor/', site_views.mentor, name='mentor'),
                path('single_grow/<int:grow_id>/', site_views.single_grow, name='boijigch'),
                path('bodlogo/', site_views.bodlogo, name='bodlogo'),
                path('mentorlist/<int:office_id>/', site_views.mentor_list, name='mentor'),
                path('news/', site_views.news, name='blog'),
                path('single_news/<int:article_id>/', site_views.single_news, name='blog1'),
                path('incubator/', site_views.incubator, name='incubator1'),
                path('submission/', site_views.incubator_request2, name='incubator'),
                path('incubator_request/', site_views.incubator_request, name='incubator'),
                path('gcapcha/', site_views.gcapcha, name='incubator2'),
                path('investor/', site_views.investor, name='investor'),
                path('biz_stepup/', site_views.biz_stepup, name='biz'),
                path('i_stepup/', site_views.i_stepup, name='istep'),
                path('pitchday/', site_views.coworking, name='pitchDay'),
                path('contact/', site_views.contact, name='contact'),
                path('sendmail/', site_views.sendmail, name='contact1'),
                path('slider/', site_views.slider, name='slider'),
                path('results/', site_views.results, name='result'),
                path('check_email/', site_views.check_email, name='result'),
                path('check_phone/', site_views.check_phone, name='result'),
                path('pdf/<int:grow_id>/', Pdf.as_view()),
                path('test/', site_views.test, name='contact2'),
            ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
