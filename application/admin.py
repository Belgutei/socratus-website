from django.contrib import admin
from django.utils.safestring import mark_safe
from translations.admin import TranslatableAdmin, TranslationInline
from .models import *


class ImageAdmin(admin.TabularInline):
    model=Images
    fields=['image']
    extra=0


class ContentAdmin(admin.ModelAdmin):
    list_display=['title', 'publish']
    ordering=['title']
    inlines=[ImageAdmin]


class GrowtAdmin(admin.TabularInline):
    model=DrowTimeline
    fields=['ognoo', 'title', 'description']
    extra = 0

class VideoAdmin(admin.TabularInline):
    model = VideoEmbed
    fields = ['title', 'desc', 'link']
    extra = 0
    

class GrowAdmin(admin.ModelAdmin):
    list_display=['title']
    ordering=['title']
    inlines=[GrowtAdmin]


class IvalueAdmin(admin.TabularInline):
    model = Ivalue
    fields = ['image', 'description']
    extra = 0

class IncubatorAdmin(admin.ModelAdmin):
    list_display = ['title']
    ordering = ['title']
    inlines = [IvalueAdmin]

class ButetsAdmin(admin.TabularInline):
    model = Coworking_butets
    fields = ['name', 'tailbar']
    extra=0


class BolomjAdmin(admin.TabularInline):
    model = Coworking_bolomj
    fields = ['image', 'name', 'tailbar']
    extra =0


class CPriceAdmin(admin.TabularInline):
    model = CoworkingPrice
    fields = ['name','small_name','price','tailbar']
    extra=0


class CoworDesAdmin(admin.ModelAdmin):
    list_display = ['name']
    ordering =['name']
    inlines = [ButetsAdmin,BolomjAdmin]


class AdminRes(admin.ModelAdmin):
    list_display=['c1', 'created_at', 'upper_case_name']

    def upper_case_name(self, obj):
        return mark_safe("<button class='addlink'><a target='_blank' href='/pdf/%s'>PDF болгох</a></button>" % (obj.id))
        # upper_case_name.short_description = 'PDF'

class NewsImagesInline(admin.TabularInline):
    model = NewsImages
    extra = 1

class NewsAdmin(TranslatableAdmin):
    inlines = [NewsImagesInline, TranslationInline,]

class bizStepUpAdmin(admin.ModelAdmin):
    model = bizStepUp
    fields = ['title']
    extra = 0

class iStepUpAdmin(admin.ModelAdmin):
    model = iStepUp
    fields = ['title']
    extra = 0

class MentorAdmin(admin.TabularInline):
    model = Quotes
    fields = ['title', 'tailbar', 'publish']
    extra=0

class MentorDesAdmin(admin.ModelAdmin):
    list_display = ['name']
    ordering =['name']
    inlines = [MentorAdmin]

class PhilosophyAdmin(admin.ModelAdmin):
    model = Philosophy
    fields = ['title']
    extra =0

class PrincipleAdmin(admin.ModelAdmin):
    model = Principle
    fields = ['title']
    extra =0

class PhotoAdmin(admin.ModelAdmin):
    model = Photo
    fields = ['title']
    extra =0


class LogoAdmin(admin.ModelAdmin):
    model = Logo
    fields = ['title']
    extra = 0

class PitchAdmin(admin.ModelAdmin):
    model = Pitchday
    fields = ['title']
    extra = 0




admin.site.register(Content)
admin.site.register(Drow, GrowAdmin)
admin.site.register(Incubator, IncubatorAdmin)
admin.site.register(Service)
admin.site.register(Coworking, CoworDesAdmin)
admin.site.register(DrowID)
admin.site.register(News, NewsAdmin)
admin.site.register(NewsTag)
admin.site.register(NewsType)
admin.site.register(NeedFiles)
admin.site.register(NeedLinks)
admin.site.register(RequestForm, AdminRes)
admin.site.register(Contact)
admin.site.register(Algorithms)
admin.site.register(OurTeam, MentorDesAdmin)
admin.site.register(Philosophy)
admin.site.register(Principle)
admin.site.register(Photo)
admin.site.register(Logo)
admin.site.register(bizStepUp)
admin.site.register(iStepUp)
admin.site.register(Pitchday)
