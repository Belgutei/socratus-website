from django.apps import AppConfig


class ApplicationConfig(AppConfig):
    name = 'РийчПойнт ХХК'
    verbose_name = "РийчПойнт ХХК"
